package com.alimazhar;

import java.util.*;
import org.apache.commons.codec.binary.Base64;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;


public class App {

	private static final Charset UTF_8 = StandardCharsets.UTF_8;
	
	public static void main(String [] args) {
		String text = "Hello World!";
        byte[] encodedBytes = Base64.encodeBase64(text.getBytes(UTF_8));
        System.out.println("encodedBytes: " + new String(encodedBytes, UTF_8));
	}
}
